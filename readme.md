#TEAM MEMBERS:
1. Aditya Kaka
2. Avantika Mudumbai
3. Naga Srihith Penjarla
4. Nandakishore S Menon
5. Rachna Kedigehalli
6. Jahanavi Agrawal

#HOW TO COMPILE THE PROGRAM:
1. Compile the "Makefile" file with the command: "make all"
2. Run the compiled file with the command "./main"

#COMPILING THE PROGRAM:
1. Use the command "make all" on the terminal in the project directory

#WHILE USING THE PROGRAM:
1. Name of the patient can be at max 100 characters long.
2. Age should NOT have yrs as a suffix.
3. Doctor's name shouldn't exceed 100 characters.
4. Height and weight should NOT have the units as a suffix.
5. Blood group should strictly NOT have any spaces.
6. The status of the patient should strictly be 0, 1 or 2.
7. In the search function, to search on the basis of a field, the index of that field should be entered. Input should NOT be a string.
